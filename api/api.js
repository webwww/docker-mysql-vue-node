const express = require('express');
const app = express();
const axios = require('axios');
const mysql = require('mysql2');
const fs = require('fs');
const os = require('os');
const cors = require('cors')

const port = process.env.BACK_PORT || 8081;
const networkInterfaces = os.networkInterfaces();
console.log(networkInterfaces)

app.use(express.static('public'));
app.use(express.json());
app.options('*', cors())

// app.use((req, res, next) => {
//   console.log(process.env.URL_FOR_CORS)
//     res.setHeader("Access-Control-Allow-Origin", `${process.env.URL_FOR_CORS}`);
//     res.header(
//       "Access-Control-Allow-Headers",
//       "Origin, X-Requested-With, Content-Type, Accept"
//     );
//     next();
//   });

app.post('/send-form', async function(req, res){

    res.header("Access-Control-Allow-Origin", "*");

    console.log(req.body)
    const name = req.body.name;
    const comment = req.body.comment;

    if (!name || !comment) {
        res.status(400).send(false);
    };

    if (name.length > 255 || comment.length > 2000) { // длина полей в базе
      res.status(400).send("too long string");
    }
    const dbConn = await createConnection();
    const dbResponce = await insertRecord(dbConn, name, comment);

    return res.status(200).json(`Результат записан под номером ${dbResponce.insertId}`);
});

app.listen(port, () => {
    console.log('server started on', port, 'port');
})

function createConnection(){

  try {
    this.options = JSON.parse(fs.readFileSync('api/config.json'));
    console.log('host:', this.options.host)
  } catch (e) {
      console.log(e, 'Проверьте config.json');
  }

  return new Promise (function (resolve, reject) {
    const connection = mysql.createPool({
        host: this.options.host,
        user: this.options.user,
        database: this.options.database,
        password: this.options.password
    })
    resolve(connection)
  })
}

function insertRecord(connection, name, comment) {
  return new Promise(function (resolve, reject) {
      connection.promise().query(`
      insert into user_records values (default, ?, ?, UNIX_TIMESTAMP())
      `, [name, comment]).then( ([rows,fields]) => {resolve(rows)});
  })
}
